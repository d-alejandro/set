//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
#include "MASGIFLib_OCX.h"
#include <OleCtrls.hpp>
//---------------------------------------------------------------------------
class TFormSet : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *N_Help;
        TMenuItem *N_Exit;
        TGroupBox *GroupBox_Set;
        TGroupBox *GroupBox_SetA;
        TMemo *MemoA;
        TGroupBox *GroupBox3;
        TLabel *Label1;
        TEdit *Edit1;
        TUpDown *UpDown1;
        TLabel *Label2;
        TEdit *Edit2;
        TUpDown *UpDown2;
        TBitBtn *BitBtnA;
        TGroupBox *GroupBox_SetB;
        TMemo *MemoB;
        TGroupBox *GroupBox5;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *Edit3;
        TUpDown *UpDown3;
        TEdit *Edit4;
        TUpDown *UpDown4;
        TBitBtn *BitBtnB;
        TGroupBox *GroupBox_SetC;
        TMemo *MemoC;
        TGroupBox *GroupBox7;
        TLabel *Label5;
        TLabel *Label6;
        TEdit *Edit5;
        TUpDown *UpDown5;
        TEdit *Edit6;
        TUpDown *UpDown6;
        TBitBtn *BitBtnC;
        TGroupBox *GroupBox8;
        TBitBtn *BitBtnOR;
        TBitBtn *BitBtnAND;
        TBitBtn *BitBtnDIFF;
        TBitBtn *BitBtnXOR;
        TEdit *EditExpression;
        TLabel *Label7;
        TGroupBox *GroupBoxResGlobal;
        TBitBtn *BitBtnCompress;
        TGroupBox *GroupBoxRes;
        TMemo *MemoRes;
        TBevel *BevelRes;
        TBitBtn *BitBtnQuotOpen;
        TBitBtn *BitBtnQuotClose;
        TBitBtn *BitBtnBackSpace;
        TStatusBar *StatusBar;
        TBitBtn *BitBtnStart;
        TBitBtn *BitBtnStep;
        TImage *Image1;
        TCheckBox *CheckBox_StepByStep;
        TLabel *LabelA;
        TLabel *LabelB;
        TLabel *LabelC;
        TSpeedButton *SpeedButtonA;
        TSpeedButton *SpeedButtonB;
        TSpeedButton *SpeedButtonC;
        TPanel *Panel1;
        TMasGifPlayer *MasGifPlayer1;
        void __fastcall N_ExitClick(TObject *Sender);
        void __fastcall BitBtnAClick(TObject *Sender);
        void __fastcall BitBtnBClick(TObject *Sender);
        void __fastcall BitBtnCClick(TObject *Sender);
        void __fastcall N_HelpClick(TObject *Sender);
        void __fastcall BitBtnORClick(TObject *Sender);
        void __fastcall BitBtnDIFFClick(TObject *Sender);
        void __fastcall BitBtnANDClick(TObject *Sender);
        void __fastcall BitBtnQuotOpenClick(TObject *Sender);
        void __fastcall BitBtnQuotCloseClick(TObject *Sender);
        void __fastcall BitBtnCompressClick(TObject *Sender);
        void __fastcall BitBtnBackSpaceClick(TObject *Sender);
        void __fastcall EditExpressionKeyPress(TObject *Sender, char &Key);
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall BitBtnStepClick(TObject *Sender);
        void __fastcall BitBtnStartClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall SpeedButtonAClick(TObject *Sender);
        void __fastcall SpeedButtonBClick(TObject *Sender);
        void __fastcall SpeedButtonCClick(TObject *Sender);
        void __fastcall BitBtnXORClick(TObject *Sender);
private:	// User declarations
public:		// User declarations

 HRGN __fastcall ConvertBMPToRegion (Graphics::TBitmap *picture);
 void __fastcall ShowRegion (HRGN rgn);

 void __fastcall ExecEasyCommand(AnsiString EasyCommand);
 void __fastcall ConvertCommand(void);
 void __fastcall ShowError(void);

        __fastcall TFormSet(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSet *FormSet;
//---------------------------------------------------------------------------
#endif
