//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <iostream>
#include <system.hpp>
#include <stdlib.h>

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MASGIFLib_OCX"
#pragma resource "*.dfm"

  // определение типа "множество"
  typedef Set<int, 0, 50>  setArr;
  // определение множеств
  setArr A, B, C, D,
         Q, W, E, R, T, Y, U, I, O, P,
         *Temp1, *Temp2, *Temp3, *Tmp;

  bool ErrorFlag = false;
  AnsiString Command,     // команда
             EasyCommand, // подкоманда
             TmpStr;
  int PosRightDelimiter, PosLeftDelimiter,
      Flag, // номер вызова процедуры ExecEasyCommand
      i, j=0, Min, Max, Quan=10, Curr, Index;
  char Op1, Op2, Op;
  TStringList *TempList;

  TRect RectA   = TRect ( 0,   0, 150, 150),
        RectB   = TRect (80,   0, 230, 150),
        RectC   = TRect (44,  84, 194, 234),
        RectRes = TRect ( 0,   0, 250, 250);
  HRGN rgnA, rgnB, rgnC, rgnRes,
       rgnQ, rgnW, rgnE, rgnR, rgnT,
       rgnY, rgnU, rgnI, rgnO, rgnP,
       rgnTemp1, rgnTemp2, rgnTemp3, rgnTmp;
  Graphics::TBitmap *Bitmap;
  int Oper;

TFormSet *FormSet;
//---------------------------------------------------------------------------
__fastcall TFormSet::TFormSet(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::FormActivate(TObject *Sender)
{ // активизация формы

  // изображение черных окружностей поверх регионов
  Image1->Canvas->Brush->Color = clBtnFace;
  Image1->Canvas->Pen->Color = clBlack;
  Image1->Canvas->Arc(RectA.left, RectA.top, RectA.right, RectA.Bottom,
                      RectA.left+RectA.Width(), RectA.top,
                      RectA.left+RectA.Width(), RectA.top);
  Image1->Canvas->Arc(RectB.left, RectB.top, RectB.right, RectB.Bottom,
                      RectB.left+RectB.Width(), RectB.top,
                      RectB.left+RectB.Width(), RectB.top);
  Image1->Canvas->Arc(RectC.left, RectC.top, RectC.right, RectC.Bottom,
                      RectC.left+RectC.Width(), RectC.top,
                      RectC.left+RectC.Width(), RectC.top);

  // загрузка рисунка "круг"
  Bitmap = new Graphics::TBitmap();
  Bitmap->LoadFromFile("Circle.bmp");

  // создание регионов
  rgnA = ConvertBMPToRegion (Bitmap); // окружность "А"
  rgnB = ConvertBMPToRegion (Bitmap); // окружность "В"
  rgnC = ConvertBMPToRegion (Bitmap); // окружность "С"

  rgnRes = CreateRectRgn (0, 0, 250, 250); // результирующий регион

  // временные результирующие регионы
  rgnQ = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnW = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnE = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnR = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnT = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnY = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnU = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnI = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnO = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnP = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnTemp1 = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnTemp2 = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnTemp3 = CreateRectRgn (0, 0, 250, 250); // результирующий регион
  rgnTmp = CreateRectRgn (0, 0, 250, 250); // результирующий регион

  // смещение регионов
  OffsetRgn(rgnB, 80,  0); // окружность "В"
  OffsetRgn(rgnC, 44, 84); // окружность "С"

  TempList = new TStringList();
  TempList->Sorted = true;
  Flag = 0;
  D.Clear();

  // первоначальное заполнение множеств
  BitBtnAClick(this);
   Sleep(1000);
  BitBtnBClick(this);
   Sleep(1000);
  BitBtnCClick(this);
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::N_ExitClick(TObject *Sender)
{
  Close ();
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnAClick(TObject *Sender)
{ // заполнение множества А

  // заполнение списка сортированными случайными данными
  //  из диапазона [Min;Max]
  Min = UpDown1->Position;
  Max = UpDown2->Position;

  TempList->Clear();
  randomize();
  for (j=0;;) {
   TmpStr.sprintf("%02d", random (Max - Min + 1));
   if (!TempList->Find(TmpStr, Index)) {
    TempList->Add(TmpStr);
    if (++j == Quan) break;
   }
  }

  MemoA->Clear();
  MemoA->Lines = TempList;

  A.Clear();
  for (i=0;  i < TempList->Count;  i++) {
   A << StrToInt(TempList->Strings[i]);
  }

  BitBtnB->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnBClick(TObject *Sender)
{ // заполнение множества B

  // заполнение списка сортированными случайными данными
  //  из диапазона [Min;Max]
  Min = UpDown3->Position;
  Max = UpDown4->Position;

  TempList->Clear();
  randomize();
  for (j=0;;) {
   TmpStr.sprintf("%02d", random (Max - Min + 1));
   if (!TempList->Find(TmpStr, Index)) {
    TempList->Add(TmpStr);
    if (++j == Quan) break;
   }
  }
  MemoB->Clear();
  MemoB->Lines = TempList;

  B.Clear();
  for (i=0;  i < TempList->Count;  i++) {
   B << StrToInt(TempList->Strings[i]);
  }

  BitBtnC->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnCClick(TObject *Sender)
{ // заполнение множества C

  // заполнение списка сортированными случайными данными
  //  из диапазона [Min;Max]
  Min = UpDown5->Position;
  Max = UpDown6->Position;

  TempList->Clear();
  randomize();
  for (j=0;;) {
   TmpStr.sprintf("%02d", random (Max - Min + 1));
   if (!TempList->Find(TmpStr, Index)) {
    TempList->Add(TmpStr);
    if (++j == Quan) break;
   }
  }
  MemoC->Clear();
  MemoC->Lines = TempList;

  C.Clear();
  for (i=0;  i < TempList->Count;  i++) {
   C << StrToInt(TempList->Strings[i]);
  }
  BitBtnStart->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::N_HelpClick(TObject *Sender)
{
  AboutBox->ShowModal ();
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnORClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "+";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnDIFFClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "-";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnANDClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "*";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnXORClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "^";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnQuotOpenClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "(";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnQuotCloseClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + ")";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnBackSpaceClick(TObject *Sender)
{ // стирание последнего символа выражения

  EditExpression->Text = EditExpression->Text.SetLength(EditExpression->Text.Length()-1);
  EditExpression->Invalidate();
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnCompressClick(TObject *Sender)
{ // cворачивание команды

  BitBtnStart->Enabled = false;
  BitBtnCompress->Enabled = false;

  ConvertCommand(); // cворачивание команды

  if (!ErrorFlag) BitBtnStep->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::ConvertCommand(void)
{ // cворачивание команды

  // поиск первой закрывающей скобки
  PosRightDelimiter = Command.Pos(AnsiString(")"));
  if (PosRightDelimiter) { // закрывающая скобка найдена
    // поиск слева последней открывающей скобки
    AnsiString CommandLeft = Command.SubString (1, PosRightDelimiter - 1);
    PosLeftDelimiter = CommandLeft.Pos(AnsiString("("));
    if (PosLeftDelimiter) { // открывающая скобка найдена
     // выделение простой команды типа А + В
     EasyCommand = Command.SubString(PosLeftDelimiter + 1,
                     PosRightDelimiter - PosLeftDelimiter - 1);
     if (EasyCommand.Length() < 3) {
      ShowError();  return;
     };
    } else {
       ShowError();  return;
      }
  } else { // закрывающая скобка не найдена
     if (Command.Length() == 2) {
      ShowError();  return;
     }

     PosLeftDelimiter = 1;
     if (Command.Length() == 1) PosRightDelimiter = 1;
     else PosRightDelimiter = 3;
     // выделение простой команды типа А + В
     EasyCommand = Command.SubString(1,
                   PosRightDelimiter - PosLeftDelimiter + 1);
    }

  StatusBar->Panels->Items[1]->Text = EasyCommand + " -> ";

  // выделение операндов и операции
  Op1 = EasyCommand [1];
  if (EasyCommand.Length() == 3) Op2 = EasyCommand [3];
  else Op2 = ' ';
  if (EasyCommand.Length() == 3) Op = EasyCommand [2];
  else Op = '=';

  switch (Op) {
   case '+': // объединение множеств
   case '*': // перечение множеств
   case '-': // разность множеств
   case '^': // XOR множеств
   case '=': // переприваивание множеств
    break;
   default: // ошибочная операция над множествами
    ShowError();  return;
  }

  // переформирование команды Command
  Command.Delete(PosLeftDelimiter, PosRightDelimiter - PosLeftDelimiter + 1);
  switch (++Flag) {
   case 1:
    Command.Insert(AnsiString("Q"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "Q";
    break;
   case 2:
    Command.Insert(AnsiString("W"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "W";
    break;
   case 3:
    Command.Insert(AnsiString("E"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "E";
    break;
   case 4:
    Command.Insert(AnsiString("R"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "R";
    break;
   case 5:
    Command.Insert(AnsiString("T"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "T";
    break;
   case 6:
    Command.Insert(AnsiString("Y"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "Y";
    break;
   case 7:
    Command.Insert(AnsiString("Y"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "U";
    break;
   case 8:
    Command.Insert(AnsiString("U"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "I";
    break;
   case 9:
    Command.Insert(AnsiString("I"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "O";
    break;
   case 10:
    Command.Insert(AnsiString("O"), PosLeftDelimiter);
    // вывод текущей свернутой команды в строке состояния
    StatusBar->Panels->Items[1]->Text += "P";
    break;
   default:
    ShowError();  return;
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::EditExpressionKeyPress(TObject *Sender, char &Key)
{ // запрещение набора неверных символов

  switch (Key) {
   case 'A': case 'B': case 'C':
   case '(': case ')':
   case '+': case '-': case '*': case '^' : return;
  }

  if (Key == VK_ESCAPE) { BitBtnBackSpaceClick(this);  return; }

  Key = VK_ESCAPE;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::ExecEasyCommand(AnsiString EasyCommand)
{ // выполнение простой команды

  switch (Op1) { // первый операнд
   case 'A':
    Temp1 = &A;
    CombineRgn (rgnTemp1, rgnA, rgnA, RGN_COPY);
    break;
   case 'B':
    Temp1 = &B;
    CombineRgn (rgnTemp1, rgnB, rgnB, RGN_COPY);
    break;
   case 'C':
    Temp1 = &C;
    CombineRgn (rgnTemp1, rgnC, rgnC, RGN_COPY);
    break;
   case 'Q':
    Temp1 = &Q;
    CombineRgn (rgnTemp1, rgnQ, rgnQ, RGN_COPY);
    break;
   case 'W':
    Temp1 = &W;
    CombineRgn (rgnTemp1, rgnW, rgnW, RGN_COPY);
    break;
   case 'E':
    Temp1 = &E;
    CombineRgn (rgnTemp1, rgnE, rgnE, RGN_COPY);
    break;
   case 'R':
    Temp1 = &R;
    CombineRgn (rgnTemp1, rgnR, rgnR, RGN_COPY);
    break;
   case 'T':
    Temp1 = &T;
    CombineRgn (rgnTemp1, rgnT, rgnT, RGN_COPY);
    break;
   case 'Y':
    Temp1 = &Y;
    CombineRgn (rgnTemp1, rgnY, rgnY, RGN_COPY);
    break;
   case 'U':
    Temp1 = &U;
    CombineRgn (rgnTemp1, rgnU, rgnU, RGN_COPY);
    break;
   case 'I':
    Temp1 = &I;
    CombineRgn (rgnTemp1, rgnI, rgnI, RGN_COPY);
    break;
   case 'O':
    Temp1 = &O;
    CombineRgn (rgnTemp1, rgnO, rgnO, RGN_COPY);
    break;
   case 'P':
    Temp1 = &P;
    CombineRgn (rgnTemp1, rgnP, rgnP, RGN_COPY);
    break;
   default:
    ShowError();  return;
  }

  // формирование начального региона
  CombineRgn (rgnRes, rgnTemp1, rgnTemp1, RGN_COPY);
  // закраска результирующего региона на рисунке
  ShowRegion (rgnRes);
  Application->ProcessMessages();

  switch (Op2) { // второй операнд
   case 'A':
    Temp2 = &A;
    CombineRgn (rgnTemp2, rgnA, rgnA, RGN_COPY);
    break;
   case 'B':
    Temp2 = &B;
    CombineRgn (rgnTemp2, rgnB, rgnB, RGN_COPY);
    break;
   case 'C':
    Temp2 = &C;
    CombineRgn (rgnTemp2, rgnC, rgnC, RGN_COPY);
    break;
   case 'Q':
    Temp2 = &Q;
    CombineRgn (rgnTemp2, rgnQ, rgnQ, RGN_COPY);
    break;
   case 'W':
    Temp2 = &W;
    CombineRgn (rgnTemp2, rgnW, rgnW, RGN_COPY);
    break;
   case 'E':
    Temp2 = &E;
    CombineRgn (rgnTemp2, rgnE, rgnE, RGN_COPY);
    break;
   case 'R':
    Temp2 = &R;
    CombineRgn (rgnTemp2, rgnR, rgnR, RGN_COPY);
    break;
   case 'T':
    Temp2 = &T;
    CombineRgn (rgnTemp2, rgnT, rgnT, RGN_COPY);
    break;
   case 'Y':
    Temp2 = &Y;
    CombineRgn (rgnTemp2, rgnY, rgnY, RGN_COPY);
    break;
   case 'U':
    Temp2 = &U;
    CombineRgn (rgnTemp2, rgnU, rgnU, RGN_COPY);
    break;
   case 'I':
    Temp2 = &I;
    CombineRgn (rgnTemp2, rgnI, rgnI, RGN_COPY);
    break;
   case 'O':
    Temp2 = &O;
    CombineRgn (rgnTemp2, rgnO, rgnO, RGN_COPY);
    break;
   case 'P':
    Temp2 = &P;
    CombineRgn (rgnTemp2, rgnP, rgnP, RGN_COPY);
    break;
   default:
    ShowError();  return;
  }

  // формирование временного результирующего множества
  switch (Flag) {
   case  1: Tmp = &Q; break;
   case  2: Tmp = &W; break;
   case  3: Tmp = &E; break;
   case  4: Tmp = &R; break;
   case  5: Tmp = &T; break;
   case  6: Tmp = &Y; break;
   case  7: Tmp = &U; break;
   case  8: Tmp = &I; break;
   case  9: Tmp = &O; break;
   case 10: Tmp = &P; break;
  }

  // выполнение операции, в т.ч. над регионами
  switch (Op) {
   case '+': // объединение множеств
    *Tmp = *Temp1 + *Temp2;
    CombineRgn (rgnRes, rgnTemp1, rgnTemp2, RGN_OR);
    break;
   case '*': // перечение множеств
    *Tmp = *Temp1 * *Temp2;
    CombineRgn (rgnRes, rgnTemp1, rgnTemp2, RGN_AND);
    break;
   case '-': // разность множеств
    *Tmp = *Temp1 - *Temp2;
    CombineRgn (rgnRes, rgnTemp1, rgnTemp2, RGN_DIFF);
    break;
   case '^': // XOR множеств
    *Tmp = (*Temp1 - *Temp2) + (*Temp2 - *Temp1);
    CombineRgn (rgnRes, rgnTemp1, rgnTemp2, RGN_XOR);
    break;
   case '=': // переприсваивание множеств
    *Tmp = *Temp1;
    CombineRgn (rgnRes, rgnTemp1, rgnTemp2, RGN_COPY);
    break;
   default:
    ShowError();  return;
  }

  switch (Flag) {
   case  1:
    Tmp = &Q;
    CombineRgn (rgnQ, rgnRes, rgnRes, RGN_COPY);
    break;
   case  2:
    Tmp = &W;
    CombineRgn (rgnW, rgnRes, rgnRes, RGN_COPY);
    break;
   case  3:
    Tmp = &E;
    CombineRgn (rgnE, rgnRes, rgnRes, RGN_COPY);
    break;
   case  4:
    Tmp = &R;
    CombineRgn (rgnR, rgnRes, rgnRes, RGN_COPY);
    break;
   case  5:
    Tmp = &T;
    CombineRgn (rgnT, rgnRes, rgnRes, RGN_COPY);
    break;
   case  6:
    Tmp = &Y;
    CombineRgn (rgnY, rgnRes, rgnRes, RGN_COPY);
    break;
   case  7:
    Tmp = &U;
    CombineRgn (rgnU, rgnRes, rgnRes, RGN_COPY);
    break;
   case  8:
    Tmp = &I;
    CombineRgn (rgnI, rgnRes, rgnRes, RGN_COPY);
    break;
   case  9:
    Tmp = &O;
    CombineRgn (rgnO, rgnRes, rgnRes, RGN_COPY);
    break;
   case 10:
    Tmp = &P;
    CombineRgn (rgnP, rgnRes, rgnRes, RGN_COPY);
    break;
  }

  // закраска результирующего региона на рисунке фоновым цветом
  // Image1->Picture->LoadFromFile("Square.bmp");
  // закраска результирующего региона на рисунке
  ShowRegion (rgnRes);
  Application->ProcessMessages();

  // вывод промежуточного результата
  MemoRes->Clear();
  for (int i=0;  i < 32767;  i++) {
   TmpStr.sprintf("%02d", i);
   if ((*Tmp).Contains(i)) MemoRes->Lines->Add (TmpStr);
  }

  if (Command.Length() == 1) { // сворачивать больше нечего
   // вывод результата
   D = *Tmp;
   MemoRes->Clear();
   for (int i=0;  i < 32767;  i++) {
    TmpStr.sprintf("%02d", i);
    if (D.Contains(i)) MemoRes->Lines->Add (TmpStr);
   }

   Flag = 0;
   StatusBar->Panels->Items[1]->Text = "";
   BitBtnStart->Enabled = true;
   BitBtnCompress->Enabled = false;
   BitBtnStep->Enabled = false;

   StatusBar->Panels->Items[1]->Text = "Выражение вычислено!";
   Command = "";
   return;
  }

  BitBtnCompress->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnStepClick(TObject *Sender)
{ // выполнение текущей операции

  BitBtnStep->Enabled = false;
  ExecEasyCommand(EasyCommand);
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::BitBtnStartClick(TObject *Sender)
{ // старт вычисления выражения

  ErrorFlag = false;

  Image1->Picture->LoadFromFile("Square.bmp");
  Image1->Canvas->Arc(RectA.left, RectA.top, RectA.right, RectA.Bottom,
                      RectA.left+RectA.Width(), RectA.top,
                      RectA.left+RectA.Width(), RectA.top);
  Image1->Canvas->Arc(RectB.left, RectB.top, RectB.right, RectB.Bottom,
                      RectB.left+RectB.Width(), RectB.top,
                      RectB.left+RectB.Width(), RectB.top);
  Image1->Canvas->Arc(RectC.left, RectC.top, RectC.right, RectC.Bottom,
                      RectC.left+RectC.Width(), RectC.top,
                      RectC.left+RectC.Width(), RectC.top);

  MemoRes->Clear();
  StatusBar->Panels->Items[1]->Text = "";
  BitBtnStart->Enabled = false;

  Command = EditExpression->Text; // считывание команды
  BitBtnCompress->Enabled = true;

  if (!CheckBox_StepByStep->Checked)
   while (Command.Length() > 0) {
    ErrorFlag = false;
    BitBtnCompressClick(this);
     if (ErrorFlag) break;
    BitBtnStepClick(this);
     if (ErrorFlag) break;
   }
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::ShowError(void)
{ // Вывод сообщения об ошибке

  ErrorFlag = true;
  MemoRes->Clear();
  StatusBar->Panels->Items[1]->Text = "";
  ShowMessage ("В выражении синтаксическая ошибка!");
  BitBtnStart->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::SpeedButtonAClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "A";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::SpeedButtonBClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "B";
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::SpeedButtonCClick(TObject *Sender)
{
  EditExpression->Text = EditExpression->Text + "C";
}
//---------------------------------------------------------------------------
HRGN __fastcall TFormSet::ConvertBMPToRegion (Graphics::TBitmap *picture)
{
   int x = 0;
   HRGN Result = NULL;
   int height = picture->Height,
       width  = picture->Width;
   Graphics::TColor nulcolor = picture->Canvas->Pixels[0][0];

 for (int y=0;   y < height;   y++) {
   x = 0;
   while (x < width) {
    while ((x < width) && (picture->Canvas->Pixels[x][y] == nulcolor)) x++;
    if (x >= width) break;
    int XStart = x;
    while ((x < width) && (picture->Canvas->Pixels[x][y] != nulcolor))  x++;
    if (Result == NULL) {
     Result = CreateRectRgn (XStart, y, x, y+1);
    } else { CombineRgn (Result, Result, CreateRectRgn (XStart, y, x, y+1), RGN_OR);  }
   }
  }
  return Result;
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::ShowRegion (HRGN reg)
{ // показ региона

  // отображение точек региона цветом clSkyBlue
  for (int y=0;   y < 251;   y++)
   for (int x=0;   x < 251;   x++)
    if (PtInRegion(reg, x, y))
     Image1->Canvas->Pixels[x][y] = clSkyBlue;
    else
     Image1->Canvas->Pixels[x][y] = clBtnFace;

  // изображение черных окружностей поверх регионов
  Image1->Canvas->Pen->Color = clBlack;
  Image1->Canvas->Arc(RectA.left, RectA.top, RectA.right, RectA.Bottom,
                      RectA.left+RectA.Width(), RectA.top,
                      RectA.left+RectA.Width(), RectA.top);
  Image1->Canvas->Arc(RectB.left, RectB.top, RectB.right, RectB.Bottom,
                      RectB.left+RectB.Width(), RectB.top,
                      RectB.left+RectB.Width(), RectB.top);
  Image1->Canvas->Arc(RectC.left, RectC.top, RectC.right, RectC.Bottom,
                      RectC.left+RectC.Width(), RectC.top,
                      RectC.left+RectC.Width(), RectC.top);
}
//---------------------------------------------------------------------------
void __fastcall TFormSet::FormClose(TObject *Sender, TCloseAction &Action)
{ // закрытие приложения

  delete TempList, Bitmap;
}
//---------------------------------------------------------------------------

